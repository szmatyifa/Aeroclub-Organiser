var annotated_dup =
[
    [ "hu", null, [
      [ "elte", null, [
        [ "AeroClubOrganiser", null, [
          [ "controller", null, [
            [ "AuthController", "db/db7/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_auth_controller.html", "db/db7/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_auth_controller" ],
            [ "EndorsementController", "d2/dad/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_endorsement_controller.html", "d2/dad/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_endorsement_controller" ],
            [ "LicenseController", "d1/dda/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_license_controller.html", "d1/dda/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_license_controller" ],
            [ "ParachuteController", "de/df6/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_parachute_controller.html", "de/df6/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_parachute_controller" ],
            [ "PilotController", "dc/ddf/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_pilot_controller.html", "dc/ddf/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_pilot_controller" ],
            [ "PlaneController", "d7/d0f/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_plane_controller.html", "d7/d0f/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_plane_controller" ],
            [ "PlaneTypeController", "d0/d20/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_plane_type_controller.html", "d0/d20/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_plane_type_controller" ],
            [ "TransporterController", "d6/de2/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_transporter_controller.html", "d6/de2/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_transporter_controller" ]
          ] ],
          [ "model", null, [
            [ "Endorsement", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement.html", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement" ],
            [ "License", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license.html", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license" ],
            [ "Parachute", "d0/d25/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_parachute.html", "d0/d25/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_parachute" ],
            [ "Pilot", "d0/dd7/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_pilot.html", "d0/dd7/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_pilot" ],
            [ "Plane", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane" ],
            [ "PlaneType", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type.html", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type" ],
            [ "Transporter", "d7/d59/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_transporter.html", "d7/d59/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_transporter" ]
          ] ],
          [ "repository", null, [
            [ "EndorsementRepository", "d5/d22/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_endorsement_repository.html", "d5/d22/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_endorsement_repository" ],
            [ "LicenseRepository", "d5/d97/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_license_repository.html", "d5/d97/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_license_repository" ],
            [ "ParachuteRepository", "d9/d8d/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_parachute_repository.html", "d9/d8d/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_parachute_repository" ],
            [ "PilotRepository", "dc/d82/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_pilot_repository.html", "dc/d82/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_pilot_repository" ],
            [ "PlaneRepository", "d7/d3d/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_plane_repository.html", "d7/d3d/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_plane_repository" ],
            [ "PlaneTypeRepository", "da/d00/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_plane_type_repository.html", "da/d00/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_plane_type_repository" ],
            [ "TransporterRepository", "d7/d79/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_transporter_repository.html", "d7/d79/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_transporter_repository" ]
          ] ],
          [ "security", null, [
            [ "jwt", null, [
              [ "AuthEntryPointJwt", "d4/dca/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1jwt_1_1_auth_entry_point_jwt.html", "d4/dca/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1jwt_1_1_auth_entry_point_jwt" ],
              [ "AuthTokenFilter", "da/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1jwt_1_1_auth_token_filter.html", "da/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1jwt_1_1_auth_token_filter" ],
              [ "JwtUtils", "d7/dd4/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1jwt_1_1_jwt_utils.html", "d7/dd4/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1jwt_1_1_jwt_utils" ]
            ] ],
            [ "payload", null, [
              [ "request", null, [
                [ "LoginRequest", "dd/d51/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1request_1_1_login_request.html", "dd/d51/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1request_1_1_login_request" ],
                [ "SignupRequest", "d0/d55/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1request_1_1_signup_request.html", "d0/d55/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1request_1_1_signup_request" ]
              ] ],
              [ "response", null, [
                [ "JwtResponse", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response.html", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response" ],
                [ "MessageResponse", "d4/d8b/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_message_response.html", "d4/d8b/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_message_response" ]
              ] ]
            ] ],
            [ "service", null, [
              [ "PilotDetailsImpl", "d8/d31/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1service_1_1_pilot_details_impl.html", "d8/d31/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1service_1_1_pilot_details_impl" ],
              [ "PilotDetailsServiceImpl", "d2/d07/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1service_1_1_pilot_details_service_impl.html", "d2/d07/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1service_1_1_pilot_details_service_impl" ]
            ] ],
            [ "WebSecurityConfig", "dd/dbf/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1_web_security_config.html", "dd/dbf/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1_web_security_config" ]
          ] ],
          [ "service", null, [
            [ "impl", null, [
              [ "EndorsementServiceImpl", "d2/d8a/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_endorsement_service_impl.html", "d2/d8a/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_endorsement_service_impl" ],
              [ "LicenseServiceImpl", "dd/df0/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_license_service_impl.html", "dd/df0/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_license_service_impl" ],
              [ "ParachuteServiceImpl", "de/d46/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_parachute_service_impl.html", "de/d46/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_parachute_service_impl" ],
              [ "PilotServiceImpl", "d9/d08/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_pilot_service_impl.html", "d9/d08/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_pilot_service_impl" ],
              [ "PlaneServiceImpl", "d8/d30/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_plane_service_impl.html", "d8/d30/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_plane_service_impl" ],
              [ "PlaneTypeServiceImpl", "d2/d5b/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_plane_type_service_impl.html", "d2/d5b/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_plane_type_service_impl" ],
              [ "TransporterServiceImpl", "d4/d0e/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_transporter_service_impl.html", "d4/d0e/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_transporter_service_impl" ]
            ] ],
            [ "EndorsementService", "db/d2b/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_endorsement_service.html", "db/d2b/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_endorsement_service" ],
            [ "LicenseService", "df/d8d/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_license_service.html", "df/d8d/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_license_service" ],
            [ "ParachuteService", "d1/dd1/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_parachute_service.html", "d1/dd1/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_parachute_service" ],
            [ "PilotService", "d1/d03/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_pilot_service.html", "d1/d03/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_pilot_service" ],
            [ "PlaneService", "d9/d22/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_plane_service.html", "d9/d22/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_plane_service" ],
            [ "PlaneTypeService", "de/d1b/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_plane_type_service.html", "de/d1b/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_plane_type_service" ],
            [ "TransporterService", "d1/daf/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_transporter_service.html", "d1/daf/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_transporter_service" ]
          ] ],
          [ "AeroClubOrganiserApplication", "dc/d06/classhu_1_1elte_1_1_aero_club_organiser_1_1_aero_club_organiser_application.html", null ]
        ] ]
      ] ]
    ] ]
];