var classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane =
[
    [ "Plane", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a18a475c769798a0659de91e46cb17bd3", null ],
    [ "Plane", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a4ff9a99adc28fd732342f76230924905", null ],
    [ "getAirworthinessValidUntil", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a58ff8e1c6f4842bf87ce27f10c5f593b", null ],
    [ "getId", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a2c3c18eff83e6629cf0a4dc011e60b86", null ],
    [ "getInsuranceValidUntil", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a7ab41010dab605b01a08dae32b8d9b00", null ],
    [ "getRegNumber", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#aa390b82e1e7f70fce9cdb5f4c9a759f4", null ],
    [ "getResponsiblePerson", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#ac26b30c50fc560d4f7e27fdb585a49ce", null ],
    [ "getTailNumber", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#af7c1596cd9ab0b26a4d8b88940ecd850", null ],
    [ "getType", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a227b16b60490e8da53582451d40f48e7", null ],
    [ "getYearlyMaintenance", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a58bed88c95966f7470a6fc7a79521e4b", null ],
    [ "setAirworthinessValidUntil", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a6e12154050975bb115a3bd0b93305262", null ],
    [ "setId", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a9a17cca38f389c9d503aad3f079d3516", null ],
    [ "setInsuranceValidUntil", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a83ba7885216a7b96528bf97d10f0c16b", null ],
    [ "setRegNumber", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a6de2eee2086769ac58247fe2087f87fb", null ],
    [ "setResponsiblePerson", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#ae4bf5cd40484a6fd64297373c9f56be2", null ],
    [ "setTailNumber", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a31beb1b9f584139216c147b1bee7e250", null ],
    [ "setType", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a64b811d1980fd2f885b3e55fa63bc918", null ],
    [ "setYearlyMaintenance", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#aa6c474933d7e8ad50c667afeee6c4821", null ],
    [ "toString", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html#a3aaec8e4aed9a27258d5aec9a2a85152", null ]
];