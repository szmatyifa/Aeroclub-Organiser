/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Aeroclub-Organiser documentation", "index.html", [
    [ "Aeroclub-Organiser", "d6/d92/md__c___users_pepuc__desktop__programming__projekt_eszkozok__aeroclub-_organiser__aeroclub-_organiser__r_e_a_d_m_e.html", [
      [ "Funkcionális követelmények:", "d6/d92/md__c___users_pepuc__desktop__programming__projekt_eszkozok__aeroclub-_organiser__aeroclub-_organiser__r_e_a_d_m_e.html#autotoc_md1", null ],
      [ "Nem funkcionális követelmények:", "d6/d92/md__c___users_pepuc__desktop__programming__projekt_eszkozok__aeroclub-_organiser__aeroclub-_organiser__r_e_a_d_m_e.html#autotoc_md2", null ],
      [ "Szakterületi fogalomjegyzék:", "d6/d92/md__c___users_pepuc__desktop__programming__projekt_eszkozok__aeroclub-_organiser__aeroclub-_organiser__r_e_a_d_m_e.html#autotoc_md3", null ],
      [ "Szerepkörök:", "d6/d92/md__c___users_pepuc__desktop__programming__projekt_eszkozok__aeroclub-_organiser__aeroclub-_organiser__r_e_a_d_m_e.html#autotoc_md4", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"db/d2b/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_endorsement_service.html#a5f8c01286351147a89d6ce2da934675d"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';