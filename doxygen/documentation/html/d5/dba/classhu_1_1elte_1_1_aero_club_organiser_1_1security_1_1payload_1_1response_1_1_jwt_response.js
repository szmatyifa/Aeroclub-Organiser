var classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response =
[
    [ "JwtResponse", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response.html#a1e6e66914de509eb96607324aa987ac0", null ],
    [ "getAccessToken", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response.html#a90d1fe8632bb2c73064a4d5339807abb", null ],
    [ "getEmail", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response.html#a928af30f51977e1d55d1dfebeae630a8", null ],
    [ "getId", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response.html#aa6349ce9c7d317bcf680a425979c3d9f", null ],
    [ "getTokenType", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response.html#a1d635ede6305ff70981ea9eeba6b1e1f", null ],
    [ "getUsername", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response.html#ae1738411097c9636b289d5c2a1cc3e42", null ],
    [ "setAccessToken", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response.html#a408a2fd1fef0a3f4c00aecb10f9f276f", null ],
    [ "setEmail", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response.html#a4b6bf2fda56168e4da7b8d311c45d76e", null ],
    [ "setId", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response.html#a6acc8795c1f8b185d47c5ba511e8038a", null ],
    [ "setTokenType", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response.html#ace916bec579e1d05e1ee7ef64c7a563d", null ],
    [ "setUsername", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response.html#a4db72ab9a5c47e52522562196a8fe6e4", null ]
];