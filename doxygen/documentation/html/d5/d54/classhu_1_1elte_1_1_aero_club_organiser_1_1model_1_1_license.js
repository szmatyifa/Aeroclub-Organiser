var classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license =
[
    [ "License", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license.html#a258c9dc46768505907f87523407a2b87", null ],
    [ "License", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license.html#a11ee94cc656b4052f54064f68c6e3877", null ],
    [ "getDateOfInitialIssue", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license.html#a579ee063b478ab984065e2d0edbda756", null ],
    [ "getId", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license.html#ac6ebf5b6feb1ff03085c45b9a0e525c4", null ],
    [ "getLicenseNumber", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license.html#a493a714f03e9a7d9717ffb1a3057601c", null ],
    [ "getPilotId", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license.html#aed1367e16910c17268a70943edb868a3", null ],
    [ "setDateOfInitialIssue", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license.html#a959fef1b98bd7a0e14f23a531f10f648", null ],
    [ "setId", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license.html#a47f814c44a562e27a27b5b29dd505149", null ],
    [ "setLicenseNumber", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license.html#a4d1bf1455905e8b72cbd704dcee8ab0a", null ],
    [ "setPilotId", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license.html#a6cb6d436bf89037ff953133f21c0061b", null ],
    [ "toString", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license.html#adeee1b0fe6d8d9860b3a70f2a09883e4", null ]
];