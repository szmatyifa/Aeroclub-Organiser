var hierarchy =
[
    [ "hu.elte.AeroClubOrganiser.AeroClubOrganiserApplication", "dc/d06/classhu_1_1elte_1_1_aero_club_organiser_1_1_aero_club_organiser_application.html", null ],
    [ "hu.elte.AeroClubOrganiser.controller.AuthController", "db/db7/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_auth_controller.html", null ],
    [ "hu.elte.AeroClubOrganiser.model.Endorsement", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement.html", null ],
    [ "hu.elte.AeroClubOrganiser.controller.EndorsementController", "d2/dad/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_endorsement_controller.html", null ],
    [ "hu.elte.AeroClubOrganiser.service.EndorsementService", "db/d2b/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_endorsement_service.html", [
      [ "hu.elte.AeroClubOrganiser.service.impl.EndorsementServiceImpl", "d2/d8a/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_endorsement_service_impl.html", null ]
    ] ],
    [ "hu.elte.AeroClubOrganiser.security.payload.response.JwtResponse", "d5/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_jwt_response.html", null ],
    [ "hu.elte.AeroClubOrganiser.security.jwt.JwtUtils", "d7/dd4/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1jwt_1_1_jwt_utils.html", null ],
    [ "hu.elte.AeroClubOrganiser.model.License", "d5/d54/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_license.html", null ],
    [ "hu.elte.AeroClubOrganiser.controller.LicenseController", "d1/dda/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_license_controller.html", null ],
    [ "hu.elte.AeroClubOrganiser.service.LicenseService", "df/d8d/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_license_service.html", [
      [ "hu.elte.AeroClubOrganiser.service.impl.LicenseServiceImpl", "dd/df0/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_license_service_impl.html", null ]
    ] ],
    [ "hu.elte.AeroClubOrganiser.security.payload.request.LoginRequest", "dd/d51/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1request_1_1_login_request.html", null ],
    [ "hu.elte.AeroClubOrganiser.security.payload.response.MessageResponse", "d4/d8b/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1response_1_1_message_response.html", null ],
    [ "hu.elte.AeroClubOrganiser.model.Parachute", "d0/d25/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_parachute.html", null ],
    [ "hu.elte.AeroClubOrganiser.controller.ParachuteController", "de/df6/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_parachute_controller.html", null ],
    [ "hu.elte.AeroClubOrganiser.service.ParachuteService", "d1/dd1/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_parachute_service.html", [
      [ "hu.elte.AeroClubOrganiser.service.impl.ParachuteServiceImpl", "de/d46/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_parachute_service_impl.html", null ]
    ] ],
    [ "hu.elte.AeroClubOrganiser.model.Pilot", "d0/dd7/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_pilot.html", null ],
    [ "hu.elte.AeroClubOrganiser.controller.PilotController", "dc/ddf/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_pilot_controller.html", null ],
    [ "hu.elte.AeroClubOrganiser.service.PilotService", "d1/d03/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_pilot_service.html", [
      [ "hu.elte.AeroClubOrganiser.service.impl.PilotServiceImpl", "d9/d08/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_pilot_service_impl.html", null ]
    ] ],
    [ "hu.elte.AeroClubOrganiser.model.Plane", "dd/dfa/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane.html", null ],
    [ "hu.elte.AeroClubOrganiser.controller.PlaneController", "d7/d0f/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_plane_controller.html", null ],
    [ "hu.elte.AeroClubOrganiser.service.PlaneService", "d9/d22/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_plane_service.html", [
      [ "hu.elte.AeroClubOrganiser.service.impl.PlaneServiceImpl", "d8/d30/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_plane_service_impl.html", null ]
    ] ],
    [ "hu.elte.AeroClubOrganiser.model.PlaneType", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type.html", null ],
    [ "hu.elte.AeroClubOrganiser.controller.PlaneTypeController", "d0/d20/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_plane_type_controller.html", null ],
    [ "hu.elte.AeroClubOrganiser.service.PlaneTypeService", "de/d1b/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_plane_type_service.html", [
      [ "hu.elte.AeroClubOrganiser.service.impl.PlaneTypeServiceImpl", "d2/d5b/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_plane_type_service_impl.html", null ]
    ] ],
    [ "hu.elte.AeroClubOrganiser.security.payload.request.SignupRequest", "d0/d55/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1payload_1_1request_1_1_signup_request.html", null ],
    [ "hu.elte.AeroClubOrganiser.model.Transporter", "d7/d59/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_transporter.html", null ],
    [ "hu.elte.AeroClubOrganiser.controller.TransporterController", "d6/de2/classhu_1_1elte_1_1_aero_club_organiser_1_1controller_1_1_transporter_controller.html", null ],
    [ "hu.elte.AeroClubOrganiser.service.TransporterService", "d1/daf/interfacehu_1_1elte_1_1_aero_club_organiser_1_1service_1_1_transporter_service.html", [
      [ "hu.elte.AeroClubOrganiser.service.impl.TransporterServiceImpl", "d4/d0e/classhu_1_1elte_1_1_aero_club_organiser_1_1service_1_1impl_1_1_transporter_service_impl.html", null ]
    ] ],
    [ "AuthenticationEntryPoint", null, [
      [ "hu.elte.AeroClubOrganiser.security.jwt.AuthEntryPointJwt", "d4/dca/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1jwt_1_1_auth_entry_point_jwt.html", null ]
    ] ],
    [ "JpaRepository", null, [
      [ "hu.elte.AeroClubOrganiser.repository.EndorsementRepository", "d5/d22/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_endorsement_repository.html", null ],
      [ "hu.elte.AeroClubOrganiser.repository.LicenseRepository", "d5/d97/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_license_repository.html", null ],
      [ "hu.elte.AeroClubOrganiser.repository.ParachuteRepository", "d9/d8d/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_parachute_repository.html", null ],
      [ "hu.elte.AeroClubOrganiser.repository.PilotRepository", "dc/d82/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_pilot_repository.html", null ],
      [ "hu.elte.AeroClubOrganiser.repository.PlaneRepository", "d7/d3d/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_plane_repository.html", null ],
      [ "hu.elte.AeroClubOrganiser.repository.PlaneTypeRepository", "da/d00/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_plane_type_repository.html", null ],
      [ "hu.elte.AeroClubOrganiser.repository.TransporterRepository", "d7/d79/interfacehu_1_1elte_1_1_aero_club_organiser_1_1repository_1_1_transporter_repository.html", null ]
    ] ],
    [ "OncePerRequestFilter", null, [
      [ "hu.elte.AeroClubOrganiser.security.jwt.AuthTokenFilter", "da/dba/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1jwt_1_1_auth_token_filter.html", null ]
    ] ],
    [ "UserDetails", null, [
      [ "hu.elte.AeroClubOrganiser.security.service.PilotDetailsImpl", "d8/d31/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1service_1_1_pilot_details_impl.html", null ]
    ] ],
    [ "UserDetailsService", null, [
      [ "hu.elte.AeroClubOrganiser.security.service.PilotDetailsServiceImpl", "d2/d07/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1service_1_1_pilot_details_service_impl.html", null ]
    ] ],
    [ "WebSecurityConfigurerAdapter", null, [
      [ "hu.elte.AeroClubOrganiser.security.WebSecurityConfig", "dd/dbf/classhu_1_1elte_1_1_aero_club_organiser_1_1security_1_1_web_security_config.html", null ]
    ] ]
];