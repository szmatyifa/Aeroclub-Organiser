var classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement =
[
    [ "Endorsement", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement.html#afe5598df9104aef499a67c8fcee87f3c", null ],
    [ "Endorsement", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement.html#aad9126bd0e2034fc8735a5975976000a", null ],
    [ "getId", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement.html#acacb55f276527c7e56246f845c66c19f", null ],
    [ "getPilot", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement.html#ab03bbbb8d8d320e708b03ff7f7db8ec4", null ],
    [ "getPlaneTypeDate", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement.html#ad6e75343605327b1ce495fd3050fb202", null ],
    [ "getType", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement.html#a2faeb93fb793b9b8ee9a48600a950ee4", null ],
    [ "setId", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement.html#ae4057ccbed046b79b8f3c8eb5d0fd075", null ],
    [ "setPilot", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement.html#ace84fcf924d448040fa8ee46b73ef24f", null ],
    [ "setPlaneTypeDate", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement.html#aaef85183aa36970b246df234ca625bad", null ],
    [ "setType", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement.html#a2d5356cc4cd9f0e4c4c4ea4bd202a2c9", null ],
    [ "toString", "d4/d79/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_endorsement.html#a7ea0d59151a7c522aeada75bf488badc", null ]
];