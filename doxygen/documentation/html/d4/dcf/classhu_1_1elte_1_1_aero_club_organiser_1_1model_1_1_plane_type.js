var classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type =
[
    [ "PlaneType", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type.html#ab84d8dc2418adace12351253d82fafe5", null ],
    [ "PlaneType", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type.html#a6708bc2ee1c63f5e7b83c3abaa7f658a", null ],
    [ "getEndorsement", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type.html#aae227191c102a598554be49a1642e640", null ],
    [ "getId", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type.html#a34b441811c5b020d026750884bfa5ce7", null ],
    [ "getPlanes", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type.html#a6b955dd55dea5f0603ba155d7daca4f7", null ],
    [ "getType", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type.html#ac17953cc3662e2633e85a08900a84e3f", null ],
    [ "setEndorsement", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type.html#a256d8a80a3d88fe33648f6410e3effde", null ],
    [ "setId", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type.html#ae7129edf67ac680f3312ee6fdb484a88", null ],
    [ "setPlanes", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type.html#ac5deb71f8786fb280343f3b9b6bd9456", null ],
    [ "setType", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type.html#a457e733cb8b264e7be60b5ffedb34d3c", null ],
    [ "toString", "d4/dcf/classhu_1_1elte_1_1_aero_club_organiser_1_1model_1_1_plane_type.html#a5f2b12ab64b03b9bdec9172a63612fc6", null ]
];