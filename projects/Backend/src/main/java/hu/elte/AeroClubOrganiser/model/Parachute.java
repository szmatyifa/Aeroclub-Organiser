package hu.elte.AeroClubOrganiser.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
///Public class for the Parachute
public class Parachute {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Pilot responsiblePerson;

    private String type;
    private String serialNumber;
    private String lifeTimeValidity;
    private LocalDate foldingDate;

    public Parachute() {
    }

    public Parachute(long id, Pilot responsiblePerson, String type, String serialNumber,
                     String lifeTimeValidity, LocalDate foldingDate) {
        this.id = id;
        this.responsiblePerson = responsiblePerson;
        this.type = type;
        this.serialNumber = serialNumber;
        this.lifeTimeValidity = lifeTimeValidity;
        this.foldingDate = foldingDate;
    }

    ///Method for getting the ID
    public long getId() {
        return id;
    }

    ///Method for setting the ID
    public void setId(long id) {
        this.id = id;
    }

    ///Method for getting the ResponsiblePerson
    public Pilot getResponsiblePerson() {
        return responsiblePerson;
    }

    ///Method for setting the ResponsiblePerson
    public void setResponsiblePerson(Pilot responsiblePerson) {
        this.responsiblePerson = responsiblePerson;
    }

    ///Method for getting the Type
    public String getType() {
        return type;
    }

    ///Method for setting the Type
    public void setType(String type) {
        this.type = type;
    }

    ///Method for getting the SerialNumber
    public String getSerialNumber() {
        return serialNumber;
    }

    ///Method for setting the SerialNumber
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    ///Method for getting the LifeTimeValidity
    public String getLifeTimeValidity() {
        return lifeTimeValidity;
    }

    ///Method for setting the LifeTimeValidity
    public void setLifeTimeValidity(String lifeTimeValidity) {
        this.lifeTimeValidity = lifeTimeValidity;
    }

    ///Method for getting the FoldingDate
    public LocalDate getFoldingDate() {
        return foldingDate;
    }

    ///Method for setting the FoldingDate
    public void setFoldingDate(LocalDate foldingDate) {
        this.foldingDate = foldingDate;
    }

    @Override
    public String toString() {
        return "Parachute{" +
                "responsiblePerson=" + responsiblePerson +
                ", type='" + type + '\'' +
                ", serialNumber='" + serialNumber + '\'' +
                ", lifeTimeValidity='" + lifeTimeValidity + '\'' +
                ", foldingDate=" + foldingDate +
                '}';
    }
}
